#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 11:37:57 2019

@author: vaishnavi
"""
def find_lower_limit(n):
    return int(''.join(str(i) for i in range(1, n + 1)))

def find_upper_limit(n):
    return int(''.join(str(i) for i in range(10 - n, 10)))

def gen_limits(n):
    start = find_lower_limit(n)
    end = find_upper_limit(n)
    return start,end

def sorted_val(val):
    return int(''.join(sorted(str(val))))

def is_valid_value(val):
    return len(set(str(val))) == len(str(val)) and sorted_val(val) == val

def gen_odometer_values(n):
    start,end = gen_limits(n)
    return [i for i in range(start, end + 1) if is_valid_value(i)]

def find_next_prev_val(odo_list, val, jump):
    n = len(odo_list)
    return odo_list[(odo_list.index(val) + jump) % n]

def find_next_prev_n_vals(odo_list, val, n, jump):
    val_list = []
    for i in range(0, n):
       val_list.append(find_next_prev_val(odo_list, val, jump))
       val = val_list[-1]
    return val_list

def dist_btw_vals(odo_list, val1, val2):
    return (odo_list.index(val2) - odo_list.index(val1)) % len(odo_list)
    
size = 4
odometer_vals = gen_odometer_values(size)
given_val = 5789
NEXT = 1
PREV = -1
n = 5
print(find_next_prev_n_vals(odometer_vals, given_val, n, NEXT))
print(dist_btw_vals(odometer_vals,5689,1248))