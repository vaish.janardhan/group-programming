#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 09:47:49 2019

@author: vaishnavi
"""
def extract_roll_marks(file_name):
    file = open(file_name, "r")
    content = file.readlines()
    roll_marks_dict = {}
    for line in content:
        details = line.split()
        roll_marks_dict[details[0]] = int(details[1])
    return roll_marks_dict

def sort_mark_list(file_name):
    mark_list = extract_roll_marks(file_name)
    sorted_list = sorted(mark_list.items(),
                         key=lambda kv: kv[1], reverse = True)
    return sorted_list

def gen_rank_list(file_name):
    mark_list = sort_mark_list(file_name)
    MAX_MARK = mark_list[0][1]
    rank = 1
    num = 1
    ranks = []
    for record in mark_list:
        record_list = list(record)
        if record_list[1] != MAX_MARK:
            rank = num
            MAX_MARK = record_list[1]
        record_list.append(rank)
        num += 1
        ranks.append(record_list)
    return ranks

import sys
print(gen_rank_list(sys.argv[1]))